
from utils import base_handlers as bh

class IndexHandler(bh.StandardHandler):

    def get(self):
        self.render('index.html')


class EndpointExampleHandler(bh.RestHandler):

    def post(self):
        self.send_json(self.getJson())
