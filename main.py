
# Adding external libs to the pythonpath
import sys
if 'libs' not in sys.path:
    sys.path[0:0] = ['libs']

##

import webapp2

from routes import ROUTES

app = webapp2.WSGIApplication(ROUTES, debug=True)

