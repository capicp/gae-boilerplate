
import webapp2
from webapp2_extras import jinja2
from jinja2.runtime import TemplateNotFound

import json


class StandardHandler(webapp2.RequestHandler):

    @webapp2.cached_property
    def jinja2(self):
        """Returns a Jinja2 renderer cached in the app registry"""
        return jinja2.get_jinja2(app=self.app)

    def render(self, template_name, template_vars={}):
        # Preset values for the template
        values = {}

        # Add manually supplied template values
        values.update(template_vars)

        # read the template or 404.html
        try:
            self.response.write(self.jinja2.render_template(template_name, **values))
        except TemplateNotFound:
            self.abort(404)

class RestHandler(webapp2.RequestHandler):

    __jsonData = None

    def dispatch(self):
        
        try:        
            self.__jsonData = json.loads(self.request.body)
        except ValueError as e:
            # TODO: Show error and body
            pass

        super(RestHandler, self).dispatch()

    def get_json(self):
        self.response.headers['content-type'] = 'application/json'
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        return self.__jsonData

    def send_json(self, r):
        self.response.headers['content-type'] = 'application/json'
        self.response.write(json.dumps(r))

    # HTTP Method
    def options(self):
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        self.response.headers['Access-Control-Allow-Headers'] = 'Authorization, Origin, Content-Type'
        self.response.headers['Access-Control-Allow-Methods'] = 'POST, OPTIONS, GET, PUT'

