
## Run project locally

dev_appserver.py ./

## Installing dependencies

pip install -r requirements.txt -t ./libs

## Python Google App Engine Examples 

https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/appengine/standard

## TODO
* Cloud Endpoints Framework support https://cloud.google.com/endpoints/docs/frameworks/python/quickstart-frameworks-python
* Authentication base
* Sending mails functions