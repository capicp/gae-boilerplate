from webapp2 import Route

ROUTES = [

    Route('/', handler='handlers.IndexHandler', name='index'),
    Route('/api/example', handler='handlers.EndpointExampleHandler', name="api_example")
]